/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import ru.lanolin.compmath.l5.Main;
import ru.lanolin.compmath.l5.ui.controller.MainController;

public class FXMain extends Application {

	@Getter
	private Stage mainStage;
	private Scene mainScene;
	private MainController mainController;

	@Override
	public void start(Stage stage) throws Exception {
		this.mainStage = stage;
		FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResource("fx/view/MainView.fxml"));
		AnchorPane anchorPane = loader.load();
		this.mainController = loader.getController();
		this.mainController.setApplicationController(this);
		this.mainScene = new Scene(anchorPane);

		this.mainScene.getStylesheets().add("fx/css/table.css");

		stage.setScene(mainScene);
		stage.setResizable(false);
		stage.setTitle("Интерполяция");

		stage.show();
	}
}
