/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.ui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import ru.lanolin.compmath.l5.math.Formulas;
import ru.lanolin.compmath.l5.ui.FXMain;
import ru.lanolin.compmath.l5.ui.TablePreview;
import ru.lanolin.compmath.l5.utils.AlertShow;
import ru.lanolin.compmath.l5.utils.ReadFromCSV;
import ru.lanolin.compmath.l5.utils.Result;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;

public class MainController {

	private FXMain application;
	public void setApplicationController(FXMain application){
		this.application = application;
	}

	@FXML
	public TableView<TablePreview> tableSeries;
	@FXML
	public TextField varX;
	@FXML
	public TextField varY;
	@FXML
	public ChoiceBox<Formulas> choiceFormula;

	@FXML
	private Button choiceFile;
	@FXML
	private Button solveButton;

	@FXML
	private TextField pathToFile;

	private FileChooser chooser;
	private XYSeries series;
	private ObservableList<TablePreview> items;

	@FXML
	public void initialize(){
		chooser = new FileChooser();
		FileChooser.ExtensionFilter csv = new FileChooser.ExtensionFilter("CSV (*.csv)", "*.csv");
		chooser.getExtensionFilters().add(csv);
		solveButton.setDisable(true);
		initTable();
		initChoiceMethod();
	}

	private void initChoiceMethod() {
		ObservableList<Formulas> forms = FXCollections.observableArrayList(Formulas.values());
		choiceFormula.setItems(forms);
		choiceFormula.getSelectionModel().select(0);
	}

	private void initTable() {
		items = FXCollections.observableArrayList();
		TableColumn<TablePreview, String> x = new TableColumn<>("X");
		x.setCellValueFactory(new PropertyValueFactory<>("x"));
		TableColumn<TablePreview, String> y = new TableColumn<>("Y");
		y.setCellValueFactory(new PropertyValueFactory<>("y"));

		tableSeries.getColumns().add(x);
		tableSeries.getColumns().add(y);
		tableSeries.setItems(items);
	}

	@FXML
	private void actionChoiceButton(ActionEvent actionEvent){
		File data = chooser.showOpenDialog(application.getMainStage());
		if(data == null) return;
		pathToFile.setText(data.getAbsolutePath());
		try {
			series = ReadFromCSV.read(data);
			items.clear();
			for(int i = 0; i < series.getN(); i++){
				TablePreview tp = new TablePreview();
				tp.setX(series.getXi(i).toString());
				tp.setY(series.getYi(i).toString());
				items.add(tp);
			}
			solveButton.setDisable(false);
		}catch (IOException | NumberFormatException e) {
			AlertShow.showAlert(Alert.AlertType.ERROR, "Ошибка чтения", ButtonType.CLOSE, "Ошибка чтения", e.toString(), e.getLocalizedMessage());
			series = null;
			items.clear();
			solveButton.setDisable(true);
		}
	}

	@FXML
	public void solve(ActionEvent actionEvent) {
		BigDecimal x;
		try{
			x = new BigDecimal(varX.getText().replace(',','.'));
		}catch (NumberFormatException exc){
			AlertShow.showAlert(Alert.AlertType.ERROR, "Ошибка ввода", ButtonType.OK, "Ошибка ввода X", exc.toString(), "Не верно введено число X");
			return;
		}

		try{
			Result result = choiceFormula.getSelectionModel().getSelectedItem().getFormula().solve(series, x);
			AlertShow.showAlert(Alert.AlertType.INFORMATION, "Найдено решение", ButtonType.APPLY, "Найдено y=f(x)", null, result.toString());
			varY.setText(result.getY().toString());
		}catch (Exception e){
			AlertShow.showAlert(Alert.AlertType.ERROR, "Ошибка", ButtonType.CLOSE, "Ошибка при решении", e.toString(), e.getLocalizedMessage());
		}
	}

}
