/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.ui;

import javafx.beans.property.SimpleStringProperty;
import lombok.Data;

public class TablePreview {

	private SimpleStringProperty x;
	private SimpleStringProperty y;

	public TablePreview() {
		x = new SimpleStringProperty("");
		y = new SimpleStringProperty("");
	}

	public void setX(String x) {
		this.x.set(x);
	}

	public void setY(String y) {
		this.y.set(y);
	}


	public String getX() {
		return x.get();
	}

	public SimpleStringProperty xProperty() {
		return x;
	}

	public String getY() {
		return y.get();
	}

	public SimpleStringProperty yProperty() {
		return y;
	}
}
