/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.math;

import ru.lanolin.compmath.l5.utils.Result;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.math.BigDecimal;

public interface FormulaImpl {

	Result solve(XYSeries series, BigDecimal x);

}
