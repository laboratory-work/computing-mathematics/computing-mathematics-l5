/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.math;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.lanolin.compmath.l5.utils.Result;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Многочлен Лагранжа
 * Интерполяционный полином Ln(x), степени не больше n
 * @author lanolin
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class LagrangeFormula implements FormulaImpl{

	/**
	 * Многочлен Лагранжа. Находит значение
	 * @param series {@link XYSeries} - серия известных значений функций
	 * @return {@link Result}
	 */
	@Override
	public Result solve(XYSeries series, BigDecimal x) {
		Result ans = new Result();
		int n = series.getN();

		BigDecimal ln = IntStream.range(0, n)
				.mapToObj(i -> series.getYi(i).multiply(multiplier(series, x, series.getXi(i))))
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		ans.setX(x);
		ans.setY(ln);
		return ans;
	}

	private BigDecimal multiplier(XYSeries series, BigDecimal x, BigDecimal xi){
		BigDecimal numerator = Arrays.stream(series.getXi())
				.filter(xf -> xf.compareTo(xi) != 0)
				.map(BigDecimal::negate)
				.map(xj -> xj.add(x))
				.reduce(BigDecimal.ONE, BigDecimal::multiply);

		BigDecimal denominator = Arrays.stream(series.getXi())
				.filter(xf -> xf.compareTo(xi) != 0)
				.map(BigDecimal::negate)
				.map(xj -> xj.add(xi))
				.reduce(BigDecimal.ONE, BigDecimal::multiply);

		return numerator.divide(denominator, MathContext.DECIMAL128);
	}
}
