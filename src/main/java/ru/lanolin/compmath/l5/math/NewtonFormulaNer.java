/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.math;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.lanolin.compmath.l5.utils.Result;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * @author lanolin
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class NewtonFormulaNer implements FormulaImpl {

	@Override
	public Result solve(XYSeries series, BigDecimal x) {
		Result ans = new Result();
		int n = series.getN();

		int I = 0;
		for(int i = 0; i < n; i++){
			if(series.getXi(i).compareTo(x) > 0){
				I = i;
				break;
			}
		}

		if(I - 2 < 0){
			I = 2;
		}

		if(I+2 > series.getN()){
			I = I-1;
		}

		int i1 = I-1;


		BigDecimal yi = series.getYi(i1);
		BigDecimal f1s = f1(series, i1);
		BigDecimal f2s = f2(series, i1);
		BigDecimal negate = series.getXi(i1).negate();
		BigDecimal xadd = x.add(negate);
		BigDecimal f1sAdd = f1s.multiply(xadd);
		BigDecimal negate1 = series.getXi(i1 + 1).negate();
		BigDecimal x2add = x.add(negate1);
		BigDecimal multiply = f2s.multiply(xadd).multiply(x2add);
		BigDecimal Nn1 = yi
				.add(f1sAdd)
				.add(multiply);

		int i2 = I-2;
		BigDecimal Nn2 = series.getYi(i2)
				.add(f1(series, i2).multiply(x.add(series.getXi(i2).negate())))
				.add(f2(series, i2)
						.multiply(x.add(series.getXi(i2).negate()))
						.multiply(x.add(series.getXi(i2 + 1).negate()))
				);

		BigDecimal y = Nn1.add(Nn2).divide(BigDecimal.valueOf(2), MathContext.DECIMAL128);

		ans.setX(x);
		ans.setY(y);
		return ans;
	}

	private BigDecimal f1(XYSeries series, int i){
		BigDecimal numerator = series.getYi(i+1).add(series.getYi(i).negate());
		BigDecimal denominator = series.getXi(i+1).add(series.getXi(i).negate());
		return numerator.divide(denominator, MathContext.DECIMAL128);
	}

	private BigDecimal f2(XYSeries series, int i){
		BigDecimal numerator = f1(series, i + 1).add(f1(series, i).negate());
		BigDecimal denominator = series.getXi(i+2).add(series.getXi(i).negate());
		return numerator.divide(denominator, MathContext.DECIMAL128);
	}
}
