/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.math;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @version 1.0
 * @author lanolin
 */
@AllArgsConstructor
@Getter
public enum Formulas {
	Lagrange("Формула Лагранжа", new LagrangeFormula()),
	Newton("Формула Ньютона для равностоящих узлов", new NewtonFormula()),
	NewtonNer("Формула Ньютона", new NewtonFormulaNer());

	private final String name;
	private final FormulaImpl formula;


	@Override
	public String toString() {
		return name;
	}
}
