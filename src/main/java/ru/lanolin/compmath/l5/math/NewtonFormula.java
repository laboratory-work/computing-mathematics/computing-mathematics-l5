/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.math;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.lanolin.compmath.l5.utils.BigDecimalUtils;
import ru.lanolin.compmath.l5.utils.Result;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * @author lanolin
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class NewtonFormula implements FormulaImpl{

	@Override
	public Result solve(XYSeries series, BigDecimal x) {
		Result ans = new Result();

		final int n = series.getN();

		int I = -1;
		BigDecimal h = series.getXi(0).negate().add(series.getXi(1));
		for (int i = 0; i < n - 1; i++) {
			BigDecimal hi = series.getXi(i).negate().add(series.getXi(i+1));
			if(h.compareTo(hi) != 0){ throw new IllegalArgumentException("Не равностоящие узловые точки"); }
			if(series.getXi(i+1).compareTo(x) > 0 && I == -1){ I = i+1; }
		}

		BigDecimal y = I <= (n >> 1) ? forward(series, x, h, 0) : back(series, x, h, I);

		ans.setX(x);
		ans.setY(y);
		return ans;
	}

	private BigDecimal deltaY(XYSeries series, int pow, int i){
		if(i+1 >= series.getN()){
			return null;
		}
		if(pow == 1){
			return series.getYi(i+1).add(series.getYi(i).negate());
		}

		BigDecimal delta1 = deltaY(series, pow - 1, i + 1);
		BigDecimal delta2 = deltaY(series, pow - 1, i);

		if(delta1 == null || delta2 == null){
			return BigDecimal.ZERO;
		}

		return delta1.add(delta2.negate());
	}


	private BigDecimal forward(XYSeries series, BigDecimal x, BigDecimal h, int start){
		int steps = series.getN() - start - 1;
		BigDecimal t = x.add(series.getXi(start).negate()).divide(h, MathContext.DECIMAL128);
		BigDecimal y = series.getYi(start);

		for (int i = 0; i < steps; i++) {
			BigDecimal delta = deltaY(series, i+1, start);
			BigDecimal numerator = new BigDecimal(t.toString());
			for (int j = 0; j < i; j++) {
				numerator = numerator.multiply(t.add(BigDecimal.valueOf(j+1).negate()));
			}
			BigDecimal denominator = BigDecimalUtils.factor(BigDecimal.valueOf(i+1));
			y = y.add(numerator.divide(denominator, MathContext.DECIMAL128).multiply(delta));
		}

		return y;
	}

	private BigDecimal back(XYSeries series, BigDecimal x, BigDecimal h, int start){
		int steps = start;
		int N = series.getN() - 1;
		BigDecimal t = x.add(series.getXi(N).negate()).divide(h, MathContext.DECIMAL128);
		BigDecimal y = series.getYi(N);

		for (int i = 0; i < steps; i++) {
			BigDecimal delta = deltaY(series, i+1, N-i-1);
			BigDecimal numerator = new BigDecimal(t.toString());
			for (int j = 0; j < i; j++) {
				numerator = numerator.multiply(t.add(BigDecimal.valueOf(j+1)));
			}
			BigDecimal denominator = BigDecimalUtils.factor(BigDecimal.valueOf(i+1));
			BigDecimal divide = numerator.divide(denominator, MathContext.DECIMAL128);
			BigDecimal addic = divide.multiply(delta);
			System.out.println(i + " " + delta);
			y = y.add(addic);
		}

		return y;
	}
}
