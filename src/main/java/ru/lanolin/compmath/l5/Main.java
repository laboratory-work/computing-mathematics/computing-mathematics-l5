/*
 * Copyright (c) 2020.  All right reserved.
 */
package ru.lanolin.compmath.l5;

import javafx.application.Application;
import lombok.SneakyThrows;
import ru.lanolin.compmath.l5.math.Formulas;
import ru.lanolin.compmath.l5.ui.FXMain;
import ru.lanolin.compmath.l5.utils.ReadFromCSV;
import ru.lanolin.compmath.l5.utils.Result;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.math.BigDecimal;

/**
 * Главная точка входа в приложение
 *
 * @version 1.0
 * @author lanolin
 */
public class Main {

	/**
	 * @param args {@link String}[] - входные параметры приложения
	 */
	@SneakyThrows
	public static void main(String[] args) {
		Application.launch(FXMain.class, args);
	}
}
