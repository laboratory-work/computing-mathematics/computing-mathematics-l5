package ru.lanolin.compmath.l5.utils;

import au.com.bytecode.opencsv.CSVReader;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ReadFromCSV {

	public static XYSeries read(String s) throws IOException {
		return read(new File(s));
	}

	public static XYSeries read(File f) throws IOException {
		FileReader fr = new FileReader(f);
		CSVReader reader = new CSVReader(fr, ',', '"', 1);

		List<String[]> vars = reader.readAll();
		XYSeries series = new XYSeries(vars.size());

		for(int i = 0; i < vars.size(); i++){
			BigDecimal xi = new BigDecimal(vars.get(i)[0].trim());
			BigDecimal yi = new BigDecimal(vars.get(i)[1].trim());
			series.set(i, xi, yi);
		}
		return series;
	}
}
