/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.utils;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
public class Result {

	private BigDecimal x;
	private BigDecimal y;

	@Override
	public String toString() {
		return "x=" + x + ",\ny=" + String.format("%.5f", y);
	}
}
