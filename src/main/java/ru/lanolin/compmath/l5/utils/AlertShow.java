/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class AlertShow {

	public static void showAlert(Alert.AlertType type, String header, ButtonType buttonType,
	                             String title, String headerText, String content){
		Alert alert = new Alert(type, header, buttonType);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(content);
		alert.show();
	}
}
