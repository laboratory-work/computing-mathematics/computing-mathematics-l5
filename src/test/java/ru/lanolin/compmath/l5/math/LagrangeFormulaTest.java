/*
 * Copyright (c) 2020.  All right reserved.
 */

package ru.lanolin.compmath.l5.math;

import junit.framework.TestCase;
import lombok.SneakyThrows;
import ru.lanolin.compmath.l5.utils.ReadFromCSV;
import ru.lanolin.compmath.l5.utils.XYSeries;

import java.math.BigDecimal;

public class LagrangeFormulaTest extends TestCase {

	@SneakyThrows
	public void test_01() {
		FormulaImpl lagrange = Formulas.Lagrange.getFormula();

		final BigDecimal x = new BigDecimal("0.253");

		XYSeries series = ReadFromCSV.read("input2.csv");

		System.out.println(lagrange.solve(series, x));

	}

}