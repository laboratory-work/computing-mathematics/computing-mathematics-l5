# Вычислительная математика. Лабораторная работа #5

## Интерполяция функции

### Цель лабораторной работы: 

решить задачу интерполяции, найти значения функции при заданных значениях аргумента, отличных от узловых точек.

Для исследования использовать:
 - линейную и квадратичную интерполяцию;
 - многочлен Лагранжа;
 - многочлен Ньютона.

### Методика проведения исследования
1. С помощью линейной и квадратичной интерполяции найти приближенное значение функции при х= Х 1 (см. табл. 1 - 4) . Подробные вычисления привести в отчете.
2. Найти приближенное значение функции при х= Х 1 (см. табл. 1 - 4) с помощью многочлена Лагранжа. Подробные вычисления привести в отчете.
3. Используя первую или вторую интерполяционную формулу Ньютона, вычислить значения функции при данных значениях аргумента (для значения Х 2 и Х 3 , см. табл. 5 - 8). Подробные вычисления привести в отчете.
4. Вычислить значения функции, используя интерполяционную формулу Ньютона для неравноотстоящих узлов (для х=Х 4 , см. табл. 1 - 4). При вычислениях учитывать только разделенные разности первого и второго порядков. Вычисления произвести дважды, используя различные узлы. Подробные вычисления привести в отчете.

### Программная реализация задачи:
1. Предусмотреть ввод исходных данных (исходные таблицы) из файла.
2. Предусмотреть ввод значения аргумента, для которого вычисляется приближенное значение функции, с клавиатуры.
3. Реализовать численные методы интерполирования (пп. 2.2, 2.3, 2.4.), каждый метод в отдельной функции/классе.
4. Предусмотреть вывод результатов на экран.

T1. 

x
0,298
0,303
0,310
0,317
0,323
0,330
0,339
y
3,2557
3,1764
3,1218
3,0482
2,9875
2,9195
2,8359

x1=0.314 x4=0.337

T5

x
0,25
0,30
0,35
0,40
0,45
0,50
0,55
y
1,2557
2,1764
3,1218
4,0482
5,9875
6,9195
7,8359

x2=0.253 x3=0.512